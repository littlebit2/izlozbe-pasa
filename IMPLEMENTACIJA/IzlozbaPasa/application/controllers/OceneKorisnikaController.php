<?php
/** 
 *  Andrić Valerija 610/14
 * Igić Lazar 389/15
 * OceneKorisnikaController – klasa za unos ocene korisnika
 * 
 * @version 1.0  
 */

class OceneKorisnikaController extends CI_Controller{
     /**
     * Index funkcija koja ucitava odgovarajuci view za unos ocene korisnika
     * 
     * @return void 
     */
    public function index(){
        $this->load->view('oceneKorisnika');
    }
     /**
     * CheckData funkcija koja proverava unesene podatke za unos ocene korisnika
     * 
     * @return void 
     */
    public function checkData(){
         $this->load->view('oceneKorisnika');
        $this->form_validation->set_rules('ocena', 'Tvoja ocena', 'required');
       
        if($this->form_validation->run()==true){
        
      
        if(isset($_POST['oceni'])){
          $select1 = $_POST['ocena'];
            switch ($select1) {
            case '1': $op=1; break;
            case '2': $op=2; break;
            case '3': $op=3; break;
            case '4': $op=4; break;
            case '5': $op=5; break;
           }
          }
          $this->load->helper('date');
        $format = "%Y-%m-%d %H:%i:%s";
        $datum=mdate($format); 
        
        $id=$_SESSION['idKor'];// id korisnika sesije //ovo je samo za primer
        
        $idPsa=$this->uri->segment(3);
        //$idPsa=$GLOBALS['idPsa'];//primer
        $this->load->model('OceneKorisnika');
        if($this->OceneKorisnika->postoji($id, $idPsa)== true){
            $this->load->view('vecGlasali');
        }else{
        $this->OceneKorisnika->addOcena($id, $idPsa,$op, $datum);
        
        $this->load->view('uspesno');
           //redirect('OcenjivanjeKorisnikController/index');
        }
        }
        //else{
          //  $this->load->view('greska');
        //}
    }
}

