<?php
/** 
 * Katarina Čegar 358/2014
 * Lazar Igić 389/2015
 * PrijavaAdminaController – klasa za prijavu administratora
 * 
 * @version 1.0  
 */


    class PrijavaAdminaController extends CI_Controller {
        public function index() {
            $this->load->view('prijava_admina');
        }
    
        
        public function odjavi($user) {
            //session_start();
            session_unset();
            //session_destroy();
            $this->load->view('home');
        }


        public function proveri() {
          
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_potvrdi');
            
            if ($this->form_validation->run()==false) {
                $this->load->view('prijava_admina');
            }else {
                
                redirect('AdminController/index');
            }
            
        }
        
        public function potvrdi() {
            
            $user=$this->input->post('username');
            $pass=$this->input->post('password');
            
            $this->load->model('PrijavaAdminaModel');
            
            if($this->PrijavaAdminaModel->prijava($user, $pass)) {
               $_SESSION['id']=$this->PrijavaAdminaModel->dohvatiIdAdmina($user, $pass);
             
                return true;
            }else {
                $this->form_validation->set_message('potvrdi', 'Incorect username or password, please try again.');
                return false;
            }
            
        }
    }

