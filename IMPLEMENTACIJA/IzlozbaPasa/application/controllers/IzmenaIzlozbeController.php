<?php
/** 
 *  Andrić Valerija 610/14
 * IzmenaIzlozbeController – klasa za dodavanje nove izložbe od strane administratora
 * 
 * @version 1.0  
 */

class IzmenaIzlozbeController extends CI_Controller{
     /**
     * Index funkcija koja ucitava odgovarajuci view za izmenu izlozbe
     * 
     * @return void 
     */
    public function index(){
        
        $this->load->view('izmenaIzlozbe');
    }
     /**
     * CheckData funkcija koja proverava unesene podatke za izmenu odgovarajuce izlozbe 
     * 
     * @return void 
     */
    public function checkData(){
        
        $this->form_validation->set_rules('nazivIzlozbe', 'Naziv izlozbe', 'required');
        $this->form_validation->set_rules('datumIzlozbe', 'Datum izlozbe', 'required');
        $this->form_validation->set_rules('lokacijaIzlozbe', 'Lokacija izlozbe', 'required');
        if($this->form_validation->run()==false){
          $this->load->view('izmenaIzlozbe');
        }
        else{
           $naziv=$this->input->post('nazivIzlozbe');
        $datum=$this->input->post('datumIzlozbe');
        $lokacija=$this->input->post('lokacijaIzlozbe');
        $idIzlozba=$this->uri->segment(3);
        
         if (isset($_POST['potvrdi'])) {
              $selected_radio = $_POST['kategorije'];
              if($selected_radio == 'grupa1'){
                  $kategorija="Grupa 1";
              }else if($selected_radio == 'grupa2'){
                   $kategorija="Grupa 2";
              }else if($selected_radio == 'grupa3'){
                  $kategorija="Grupa 3";
              }
         }
           $this->load->model('Izlozba');
           $this->Izlozba->updateIzlozba($idIzlozba,$naziv,$datum,$lokacija,$kategorija);
      
        redirect('IzlozbeAdminController/index');
        }
    }
}

