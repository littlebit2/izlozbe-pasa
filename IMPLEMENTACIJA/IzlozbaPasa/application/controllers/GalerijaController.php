<?php
/** 
 *  Andrić Valerija 610/14
 * GalerijaController – klasa za ucitavanje podataka o galeriji za izlozbe
 * 
 * @version 1.0  
 */

class GalerijaController extends CI_Controller{
 /**  
 * Index funkcija – ucitava odgovarajuci view za galeriju
 * 
 * @return void  
 */
    public function index(){
        $this->load->view('galerija');
    }
}

