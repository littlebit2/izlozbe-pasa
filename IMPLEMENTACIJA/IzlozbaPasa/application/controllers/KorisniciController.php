<?php
//Aleksa Nedeljkovic 604/14
//Klasa omogcava administartoru pregled i zabranu registrovanih korisnika
class KorisniciController extends CI_Controller 
{
    //funkcija koja iz baze pomocu modela dohvata sve registrovane korisnike i izlistava ih
    public function index() 
   {
        $this->load->model('KorisniciModel');
            
        $result1=$this->KorisniciModel->dohvatiKorisnike();
        $result['rezultati']=array();
            
        while ($row=$result1->fetch_array()) 
        {
            array_push($result['rezultati'], $row);
        }
        $this->load->view('korisnici', $result);
   }
   
   //funkcija koja omogucava zabranu korisnika brisuci ga iz baze registrovanih korisnika pomocu modela
   public function brisi()
   {
       $idKorisnika=$this->uri->segment(3);
       $this->load->model('KorisniciModel');
       $this->KorisniciModel->brisiKorisnika($idKorisnika);
       
       redirect('KorisniciController/index'); 
   }
}
