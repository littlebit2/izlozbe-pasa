<?php
//Aleksa Nedeljkovic 604/14
//Klasa prikazuje sudiji njegovu profilnu stranicu
    class SudijaController extends CI_Controller {
        //funkcija na osnovu ID-a sudije izvlaci njegove podatke iz baze pomocu modela i prikazuje ih sudiji
        public function index() {
           
            $id=$_SESSION['idSud'];
            
            $this->load->model('SudijaModel');
            $result1= $this->SudijaModel->dohvatiSudiju($id);
            $result['rezultat']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultat'], $row);
            }
             
            $this->load->view('sudija', $result);
        }
    }
