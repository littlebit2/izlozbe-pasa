<?php

class RangListaKorisniciController extends CI_Controller{
    public function index(){
        
        //$this->load->view('rangListaKorisnici');
        $this->load->model('OceneKorisnika');
        $query=$this->OceneKorisnika->rangLista();
        $result['rezultati']=array();
            
            while ($row=$query->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
          
        $this->load->view('rangListaKorisnici', $result);
    }
}