<?php
/** 
 * Katarina Čegar 358/2014
 * Igić Lazar 389/15
 * PotvrdiController – klasa koja maltene nema nikakvu funkciju
 * 
 * @version 1.0  
 */


class PotvrdiController extends CI_Controller {
    public function index() {
        $this->load->view('potvrdi');
    }
    
    public function deleteData() {
            //$this->load->view('potvrdi');
            $idClanka=$this->uri->segment(3);
            $this->load->model('IzmenaClankaModel');   //loaduje model
            $this->IzmenaClankaModel->izbrisiClanak($idClanka);
           
           redirect('ClanciAdminController/index'); 
        }
    
}
