<?php
/** 
 * Katarina Čegar 358/2014
 *
 * PrijavaZaIzlozbuController – klasa za prijavljivanje odredjenog psa za izlozbu
 * 
 * @version 1.0  
 */

class PrijavaZaIzlozbuController extends CI_Controller{
    
   public function index(){
        $this->load->model('Izlozba');
        $query=$this->Izlozba->findAllNew();
        $result['rezultati']=array();
            
            while ($row=$query->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
         $this->load->view('prijava_za_izlozbu', $result);
    }
    public function proveri() {
            
            //$this->form_validation->set_rules('izlozba', 'Izlozba', 'required');
            $this->form_validation->set_rules('kategorija', 'Kategorija', 'required');
            $this->form_validation->set_rules('rasa', 'Rasa', 'required');
            $this->form_validation->set_rules('ime', 'Ime', 'required');
            $this->form_validation->set_rules('starost', 'Starost', 'required');
            $this->form_validation->set_rules('rodovnik', 'Izlozba', 'required');
            $this->form_validation->set_rules('foto', 'Kategorija', 'required');
            
            $muski=isset($_POST['muski']);
            $zenski=isset($_POST['zenski']);
           
            
            if (($this->form_validation->run()==true) && ($muski || $zenski)){ 
                
                if (isset($_POST['Submit2'])) {
              $selected = $_POST['selekcija'];
              $this->load->model('Izlozba');
               $query=$this->Izlozba->findAllNew();
        while ($row=$query->fetch_array()) {
                if($selected == $row[naziv]){
                    $izlozba=$row[naziv];
                }
            }
              
         }

                //$izlozba=$this->input->post('izlozba');
                $kategorija=$this->input->post('kategorija');
                $rasa=$this->input->post('rasa');
                $ime=$this->input->post('ime');
                $starost=$this->input->post('starost');
                $rodovnik=$this->input->post('rodovnik');
                $foto=$this->input->post('foto');
                
                $db = mysqli_connect("localhost","root","","mydb"); //keep your db name
                $image = addslashes(file_get_contents($_FILES['images']['rodovnik']));
                //you keep your column name setting for insertion. I keep image type Blob.
                //$query = "INSERT INTO products (id,image) VALUES('','$image')";  
                //$qry = mysqli_query($db, $query);
                
                
                if($muski){
                    $pol="MUSKI";
                }else{
                    $pol="ZENSKI";
                }  
                $idk= $_SESSION['idKor'];
                $this->load->model('PrijavaZaIzlozbuModel');            
                $this->PrijavaZaIzlozbuModel->prijaviZaIzlozbu($izlozba,$kategorija, $rasa,$ime,$starost,$pol,$image,$foto,$muski,$idk);
                redirect('KorisnikController/index');
                //$this->load->view('korisnik');
                
                } else   {
               // $this->form_validation->set_message( 'Incorect input, please try again.');
                redirect('PrijavaZaIzlozbuController/index');
            }
            
        }
}
        
        
     