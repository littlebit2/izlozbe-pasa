<?php
/** 
 *  Andrić Valerija 610/14
 * IzlozbeController – klasa za ucitavanje izlozbi za korisnike
 * 
 * @version 1.0  
 */

class IzlozbeController extends CI_Controller{
 /**  
 * Index funkcija – ucitava sve izlozbe iz baze
 * 
 * @return void  
 */
 public function index(){
         $this->load->model('Izlozba');
            
            $result1=$this->Izlozba->findAll();
            $result['rezultati']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
         $this->load->view('izlozbe', $result);    

    }
    
}
