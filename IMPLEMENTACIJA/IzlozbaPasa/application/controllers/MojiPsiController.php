<?php
/** 
 *  Lazar Igić 389/2015
 *	Katarina Čegar 358/2014
 * MojiPsiController – klasa za dohvatanje pasa oredjenog korisnika
 * 
 * @version 1.0  
 */

    class MojiPsiController extends CI_Controller {
        
		/**  
		* Index funkcija – ucitava sve pse odredjenog korisnika
		* 
		* @return void  
		*/
		
		public function index() {
           
            $this->load->model('MojiPsiModel');
            
            $result1=  $this->MojiPsiModel->dohvatiPse();
            $result['rezultati']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
            $this->load->view('moji_psi', $result); 
        }

    }        
  