<?php

/** 
 * Katarina Čegar 358/2014
 * Lazar Igić 389/2015
 * KorisnikController – klasa za ulogovanog korisnika
 * 
 * @version 1.0  
 */


class KorisnikController extends CI_Controller {
/**
     * Index funkcija koja ucitava odgovarajuci view za određenog korisnika dinamički
     * 
     * @return void 
     */   

   public function index() {
           
            $id=$_SESSION['idKor'];
            
            $this->load->model('KorisnikModel');
            $result1= $this->KorisnikModel->dohvatiKorisnika($id);
            $result['rezultat']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultat'], $row);
            }
             
            $this->load->view('korisnik', $result);
        }

/**
     * odjavi funkcija koja odjavljuje određenog korisnika
     * 
     * @return void 
     */
        public function odjavi($user) {
            //session_start();
            session_unset();
            //session_destroy();
            $this->load->view('home');
        }
   
}

