<?php
//Aleksa Nedeljkovic 604/14
//Klasa omogcava registraciju korisnika
class RegistracijaKorisnikaController extends CI_Controller{
    //funkcija samo prikazuje view
    public function index() 
    {
        $this->load->view('registracija_korisnika');
    }
    //funkcija ucitava unete podatke iz forme i proverava da li su validni, zatim proverava pomocu modela da li vec
    //postoji sudija ili korisnik registrovan sa username-om koji je unet. Ukolike postoji ispisuje poruku o tome, a
    //ukoliko ne postoji unosi korisnika u bazu i prikazuje mu njegovu korisnicku stranicu
    public function checkData(){
        
        $this->form_validation->set_rules('ime', 'Ime', 'required');
        $this->form_validation->set_rules('prezime', 'Prezime', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('username', 'KorisnickoIme', 'required');
        $this->form_validation->set_rules('pass1', 'Lozinka', 'required');
        $this->form_validation->set_rules('pass2', 'Lozinka', 'required|matches[pass1]');
        $this->form_validation->set_rules('slazeSe', 'Slaganje sa pravilima', 'required');
        if($this->form_validation->run()==true){
 
        $ime=$this->input->post('ime');
        $prezime=$this->input->post('prezime');
        $email=$this->input->post('email');
        $username=$this->input->post('username');
        $pass1=$this->input->post('pass1');
        if(isset($_POST['zeliObav'])) $zeli=TRUE; else $zeli=FALSE;
              
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $result = $mysqli->query("select korisnickoIme from korisnik where korisnickoIme = '$username' UNION select korisnickoIme from sudija where korisnickoIme = '$username'");
        if ( (mysqli_num_rows($result)) >0) 
            {echo "<script type='text/javascript'>alert('VEC POSTOJI KORISNIK SA UNETIM USERNAMEOM');</script>"; 
            $this->load->view('registracija_korisnika');
            }
        else{
            
        $this->load->model('RegistracijaKorisnikaModel');
        $this->RegistracijaKorisnikaModel->registracijaKorisnika($ime, $prezime, $email, $username, $pass1, $zeli);

        $_SESSION['idKor']=$this->RegistracijaKorisnikaModel->dohvatiIdKorisnika($username, $pass1);

        redirect('KorisnikController/index');
        
        }
        
        }
        else{
            $this->load->view('registracija_korisnika');
        }
    }   
}