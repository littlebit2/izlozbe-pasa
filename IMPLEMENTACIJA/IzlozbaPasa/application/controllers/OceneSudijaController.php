<?php
/** 
 *  Andrić Valerija 610/14
 * OceneSudijaController – klasa za unos ocene sudije
 * 
 * @version 1.0  
 */

class OceneSudijaController extends CI_Controller{
     /**
     * Index funkcija koja ucitava odgovarajuci view za unos ocene sudije
     * 
     * @return void 
     */
     public function index(){
        $this->load->view('oceneSudija');
    }
    /**
     * CheckData funkcija koja proverava unesene podatke za unos ocene sudije
     * 
     * @return void 
     */
    public function checkData(){
         $this->load->view('oceneSudija');
        $this->form_validation->set_rules('ocena', 'Tvoja ocena', 'required');
       
        if($this->form_validation->run()==true){
        
      
        if(isset($_POST['oceni'])){
          $select1 = $_POST['ocena'];
            switch ($select1) {
            case '1': $op=1; break;
            case '2': $op=2; break;
            case '3': $op=3; break;
            case '4': $op=4; break;
            case '5': $op=5; break;
           }
          }
        
        $id=$_SESSION['idSud'];// id korisnika sesije //ovo je samo za primer
        
        $idPsa=$this->uri->segment(3);
        //$idPsa=$GLOBALS['idPsa'];//primer
        $this->load->model('OceneSudija');
        if($this->OceneSudija->postoji($id, $idPsa) == true){
            $this->load->view('vecGlasaliSudija');
        }else{
        $this->OceneSudija->addOcena($id, $idPsa,$op);
        
        $this->load->view('uspesnoSudija');
           //redirect('OcenjivanjeKorisnikController/index');
        }
        //}
        //else{
          //  $this->load->view('greska');
       //}
    }
}

}
