<?php
/** 
 *  Andrić Valerija 610/14
 * BrisanjeIzlozbeController – klasa za brisanje izložbe od strane administratora
 * 
 * @version 1.0  
 */

class BrisanjeIzlozbeController extends CI_Controller{
    /**
     * Index funkcija koja brise zadatu izlozbu-idIzlozbe koja se brise se prenosi preko url-a
     * 
     */
    public function index(){
         $id=$this->uri->segment(3);
         
           $this->load->model('Izlozba');
         $this->Izlozba->deleteIzlozba($id);
        
         //$this->load->library('controllers/IzlozbeAdminController');
        // $this->load->library('controllers/IzlozbeAdminController');

         redirect('IzlozbeAdminController/index');
       // $this->IzlozbeAdminController->index();

    }
   
}

