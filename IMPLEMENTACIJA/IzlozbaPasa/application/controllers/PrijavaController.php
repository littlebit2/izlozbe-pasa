<?php

/** 
 *  Katarina Čegar 358/2014
 *	Lazar Igić 389/2015	
 *
 * PrijavaController – klasa za prijavu registrovanog sudije i korisnika
 * 
 * @version 1.0  
 */

class PrijavaController extends CI_Controller {
   public function index() {
       
       if ((!isset($_SESSION['idKor'])) && !isset($GLOBALS['jeKorisnik'] ) && (!isset($_SESSION['idSud']))) {
       error_reporting(E_ERROR|E_WARNING | E_PARSE);
       $this->load->view('prijava');}
       else if (isset($_SESSION['idKor'])) { redirect ('KorisnikController/index');}
       else if ((!isset($_SESSION['idSud'])) && !isset ($GLOBALS['jeKorisnik'])) {
          error_reporting(E_ERROR|E_WARNING | E_PARSE);
          $this->load->view('prijava'); 
       }
       else  {
           redirect ('SudijaController/index');
       }
   }

    public function proveri() {
            
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_potvrdi');
            
            if ($this->form_validation->run()==false) {
                $this->load->view('prijava');
            }else {
                if($GLOBALS['jeKorisnik'] == true) {
                    //$kor=$_SESSION['idKor'];
                    redirect('KorisnikController/index');
                } else {
                    redirect('SudijaController/index');
                }
            }
            
        }
        
        public function potvrdi() {
            $GLOBALS['user']=$this->input->post('username');
            $user=$this->input->post('username');
            $pass=$this->input->post('password');
            
        
            $this->load->model('PrijavaModel');
            
            if($this->PrijavaModel->prijava($user, $pass)) {
                $GLOBALS['jeKorisnik']=true;
                $_SESSION['idKor']=$this->PrijavaModel->dohvatiIdKor($user, $pass);
             
                return true;
            }
            else if ($this->PrijavaModel->prijavaSudije($user, $pass)) {
                $GLOBALS['jeKorisnik']=false;
                $_SESSION['idSud']=$this->PrijavaModel->dohvatiIdSud($user, $pass);
                return true;
            }
            else {
                $this->form_validation->set_message('potvrdi', 'Incorect username or password, please try again.');
                return false;
            }
            
        }
   
   
        public function odjavi($user) {
            //session_start();
            session_unset();
            //session_destroy();
            $this->load->view('home');
        }
        
   }
