<?php
/** 
 *  Andrić Valerija 610/14
 * DodavanjeIzlozbeController – klasa za dodavanje nove izložbe od strane administratora
 * 
 * @version 1.0  
 */


class DodavanjeIzlozbeController extends CI_Controller{
    /**
     * Index funkcija koja ucitava odgovarajuci view za dodavanje izlozbe
     * 
     * @return void 
     */
    
    public function index(){
        $this->load->view('dodavanjeIzlozbe');
    }
    /**
     * CheckData funkcija koja proverava unesene podatke za kreiranje nove izlozbe i dodaje izlozbu u bazu
     * 
     * @return void 
     */
  
    public function checkData(){
        
        $this->form_validation->set_rules('nazivIzlozbe', 'Naziv izlozbe', 'required');
        $this->form_validation->set_rules('datumIzlozbe', 'Datum izlozbe', 'required');
        $this->form_validation->set_rules('lokacijaIzlozbe', 'Lokacija izlozbe', 'required');
        if($this->form_validation->run()==true){
        $naziv=$this->input->post('nazivIzlozbe');
        $datum=$this->input->post('datumIzlozbe');
        $lokacija=$this->input->post('lokacijaIzlozbe');
        
        
         if (isset($_POST['potvrdi'])) {
              $selected_radio = $_POST['kategorije'];
              if($selected_radio == 'grupa1'){
                  $kategorija="Grupa 1";
              }else if($selected_radio == 'grupa2'){
                   $kategorija="Grupa 2";
              }else if($selected_radio == 'grupa3'){
                  $kategorija="Grupa 3";
              }
         }
           $this->load->model('Izlozba');
           //$rez=$this->Izlozba->proveriNaziv($naziv);
           //if($rez != NULL){
             //  $this->form_validation->set_message('checkData', 'Naziv izlozbe vec postoji. Pokusajte ponovo.');
              // return;
           //}
           $this->Izlozba->addIzlozba($naziv,$datum,$lokacija,$kategorija);
      
           redirect('IzlozbeAdminController/index');
           
        }
        else{
            $this->load->view('greska');
        }
    }
    
    
}
