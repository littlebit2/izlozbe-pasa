<?php

class RangListaSudijaController extends CI_Controller{
    public function index(){
        
        
        $this->load->model('OceneSudija');
        $query=$this->OceneSudija->rangLista();
        $result['rezultati']=array();
            
            while ($row=$query->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
          
        $this->load->view('rangListaSudija', $result);
    }
}

