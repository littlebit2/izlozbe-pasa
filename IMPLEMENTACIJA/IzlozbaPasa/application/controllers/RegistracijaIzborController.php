<?php
/** 
 *  Lazar Igić 389/2015
 *	Katarina Čegar 358/2014	
 *
 * RegistracijaIzborController – klasa za izbor tipa registracije
 * 
 * @version 1.0  
 */
class RegistracijaIzborController extends CI_Controller {
    public function index() {
        $this->load->view('registracija_izbor');
    }
}