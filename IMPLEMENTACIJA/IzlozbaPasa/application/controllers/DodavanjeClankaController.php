<?php
/** 
 * Katarina Čegar 358/2014
 * Lazar Igić 389/2015
 * DodavanjeClankaController – klasa za dodavanje članaka
 * 
 * @version 1.0  
 */


    class DodavanjeClankaController extends CI_Controller {
        public function index() {
            $this->load->view('dodavanjeClanka');
        }
    
        
        public function checkData() {
            
        $this->form_validation->set_rules('naslovClanka', 'Naslov', 'required');
        $this->form_validation->set_rules('tekst', 'Tekst', 'required');
        
        
        
        if($this->form_validation->run()==false){
            $this->load->view('dodavanjeClanka');
        } else {
            
            $naslov=$this->input->post('naslovClanka');
            $tekst=$this->input->post('tekst');
            $datum=date("Y-m-d");
            $idAdmina=$_SESSION['id'];
            
            $this->load->model('DodavanjeClankaModel');   //loaduje model
            $this->DodavanjeClankaModel->dodajClanak($naslov,$tekst, $datum, $idAdmina);
            
            redirect('ClanciAdminController/index');   
        }
        
        
        
        
        
        
        }
        
        
        
        
        
        
        
        
      
    }
