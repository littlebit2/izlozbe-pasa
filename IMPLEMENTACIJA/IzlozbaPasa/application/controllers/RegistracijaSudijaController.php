<?php
//Aleksa Nedeljkovic 604/14
//Klasa omogcava registraciju sudija
class RegistracijaSudijaController extends CI_Controller {
         //funkcija samo prikazuje view
        public function index() {
            $this->load->view('registracija_sudija');
        }
        
    //funkcija ucitava unete podatke iz forme i proverava da li su validni, zatim proverava pomocu modela da li vec
    //postoji sudija ili korisnik registrovan sa username-om koji je unet. Ukolike postoji ispisuje poruku o tome, a
    //ukoliko ne postoji unosi sudiju u bazu i prikazuje mu njegovu korisnicku stranicu (SudijaController)
        public function checkData(){
        
        $this->form_validation->set_rules('ime', 'Ime', 'required');
        $this->form_validation->set_rules('prezime', 'Prezime', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('username', 'KorisnickoIme', 'required');
        $this->form_validation->set_rules('pass1', 'Lozinka', 'required');
        $this->form_validation->set_rules('pass2', 'Lozinka', 'required|matches[pass1]');
        $this->form_validation->set_rules('brlicence', 'BrojLicence', 'required');
        $this->form_validation->set_rules('licenca', 'FaliLicenca', 'required');
        $this->form_validation->set_rules('slazeSe', 'Slaganje sa pravilima', 'required');
        
        if($this->form_validation->run()==true){
 
        $ime=$this->input->post('ime');
        $prezime=$this->input->post('prezime');
        $email=$this->input->post('email');
        $username=$this->input->post('username');
        $pass1=$this->input->post('pass1');
        $brlicence=$this->input->post('brlicence');
        $licenca=$this->input->post('licenca');
        
        if(isset($_POST['zeliObav'])) $zeli=TRUE; else $zeli=FALSE;
              
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $result = $mysqli->query("select korisnickoIme from korisnik where korisnickoIme = '$username' UNION select korisnickoIme from sudija where korisnickoIme = '$username'");
        if ( (mysqli_num_rows($result)) >0) 
            {echo "<script type='text/javascript'>alert('VEC POSTOJI SUDIJA SA UNETIM USERNAMEOM');</script>"; 
            $this->load->view('registracija_sudija');
            }
        else{
        $this->load->model('RegistracijaSudijaModel');
        $this->RegistracijaSudijaModel->registracijaSudija($ime, $prezime, $email, $username, $pass1, $brlicence, $licenca, $zeli);
      
        $_SESSION['idSud']=$this->RegistracijaSudijaModel->dohvatiIdSudije($username, $pass1);
        
        redirect('SudijaController/index');
        }}
        else{
            $this->load->view('registracija_sudija');
        }
    }   
          
    }

