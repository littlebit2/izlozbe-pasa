<?php
/** 
 *  Lazar Igić 389/2015
 * RangListaController – klasa za ucitavanje izlozbi
 * 
 * @version 1.0  
 */

class RangListaController extends CI_Controller{
/**  
 * Index funkcija – ucitava sve izlozbe iz baze
 * 
 * @return void  
 */
    public function index(){
         $this->load->model('Izlozba');
            
            $result1=$this->Izlozba->findAllNew();
            $result['rezultati']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
          
        $this->load->view('birajIzlozbu', $result);
    }
     public function index1(){
         $this->load->model('Izlozba');
            
            $result1=$this->Izlozba->findAllNew();
            $result['rezultati']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
          
        $this->load->view('birajIzlozbuS', $result);
    }
}

