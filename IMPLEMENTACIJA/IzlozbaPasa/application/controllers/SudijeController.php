<?php
//Aleksa Nedeljkovic 604/14
//Klasa omogcava administartoru pregled i zabranu registrovanih sduija
class SudijeController extends CI_Controller 
{
    //funkcija koja iz baze pomocu modela dohvata sve registrovane sudije i izlistava ih
    public function index() 
   {
        $this->load->model('SudijeModel');
            
        $result1=$this->SudijeModel->dohvatiSudije();
        $result['rezultati']=array();
            
        while ($row=$result1->fetch_array()) 
        {
            array_push($result['rezultati'], $row);
        }
        $this->load->view('sudije', $result);
   }
   
   //funkcija koja omogucava zabranu sudija brisuci ga iz baze registrovanih sudija pomocu modela
   public function brisi()
   {
       $idSudija=$this->uri->segment(3);
       $this->load->model('SudijeModel');
       $this->SudijeModel->brisiSudiju($idSudija);
       
       redirect('SudijeController/index');
   }
}