<?php
/** 
 *  Lazar Igić 389/2015
 *	Katarina Čegar 358/2014
 *
 * ClanciAdminController – klasa za prikaz članaka administratoru
 * 
 * @version 1.0  
 */


    class ClanciAdminController extends CI_Controller {
        /**  
		* index funkcija – dohvata sve članke
		* 
		* @return void  
		*/
		public function index() {
              $this->load->model('ClanciModel');
            
            $result1=  $this->ClanciModel->dohvatiClanke();
            $result['rezultati']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
             
            
       
            
            
            
            
            $this->load->view('clanci_admin', $result);
        }
    }
