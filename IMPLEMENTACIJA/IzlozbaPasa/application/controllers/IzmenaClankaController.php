<?php
/** 
 * Lazar Igić 389/2015
 * IzmenaClankaController – klasa za izmenu članaka
 * 
 * @version 1.0  
 */


    class IzmenaClankaController extends CI_Controller {

		public function index() {
            //$idClanka=$_GET['idCl'];
            $this->load->view('izmenaClanka');
        }
       /**  
		* checkData funkcija – menja članke
		* 
		* @return void  
		*/
        public function checkData() {
            $this->form_validation->set_rules('naslovClanka', 'Naslov', 'required');
            $this->form_validation->set_rules('tekst', 'Tekst', 'required');
        
        
            if($this->form_validation->run()==false){
              $this->load->view('izmenaClanka');
            } else {
        
                $naslov=$this->input->post('naslovClanka');
                $tekst=$this->input->post('tekst');
                $datum=date("Y-m-d");
                $idAdmina=$_SESSION['id'];
                $idClanka=$this->uri->segment(3);
                $this->load->model('IzmenaClankaModel');   //loaduje model
                $this->IzmenaClankaModel->izmeniClanak($idClanka,$naslov,$tekst, $datum, $idAdmina);
            
            redirect('ClanciAdminController/index');   
        }       
        
        }
        /**  
		* deleteData funkcija – briše članke
		* 
		* @return void  
		*/
        public function deleteData() {
            //$idClanka=$this->uri->segment(3);
            $this->load->model('IzmenaClankaModel');   //loaduje model
            $this->IzmenaClankaModel->izbrisiClanak($idClanka);
           
        }
        
    }
