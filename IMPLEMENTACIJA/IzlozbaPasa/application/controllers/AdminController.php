<?php
/** 
 *  Lazar Igić 389/2015
 *	Katarina Čegar 358/2014
 *
 * AdminController – klasa za prikaz stranice administratora
 * 
 * @version 1.0  
 */

    class AdminController extends CI_Controller {
        public function ulogujAdmina() {
            //session_start();
            //$_SESSION["id"]=$user;
            $this->load->view('admin');
        }
        
        public function index() {
            $this->load->view('admin');
        }
    }

