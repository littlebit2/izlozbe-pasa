<!DOCTYPE html>
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/prijavaIzlozba.css' ?>">
<title>Prijava za izložbu</title>
</head>
<body>
<div class="header">
  <p><a href="<?php echo site_url('KorisnikController/index') ?>"><img src="<?php echo base_url().'images/home.png'?>" width="50" height="50" border="0" /></a></p>
</div>
<div class="body">
  <table width="80%" border="0">
    <tr>
      <td><div align="center"><img src="<?php echo base_url().'images/paw.png'?>" width="100" height="100" /></div></td>
    </tr>
    <tr>
      <td height="88"><div align="center" class="style1">PRIJAVA ZA IZLOZBU </div></td>
    </tr>
    <tr>
      <td><div align="right">
       <!-- <form action="" method="post" enctype="multipart/form-data" name="form3" id="form3">-->
            
            <?php echo validation_errors(); ?>    
            <?php echo form_open('PrijavaZaIzlozbuController/proveri');  ?>
          <table width="80%" border="0">
            <tr>
              <td width="36%" height="33"><div align="right" class="style2">Izlozba</div></td>
              <td width="64%"><label>
                <select name="selekcija"><?php foreach($rezultati as $row){
                    echo '<option value="'.$row[naziv].'">'.$row[naziv].'</option>';
                }?>
              </label></td>
            </tr>
            <tr>
              <td height="37"><div align="right" class="style2">Kategorija</div></td>
              <td><label>
                <input type="text" name="kategorija" />
              </label></td>
            </tr>
            <tr>
              <td height="32"><div align="right" class="style2">Rasa</div></td>
              <td><label>
                <input type="text" name="rasa" />
              </label></td>
            </tr>
            <tr>
              <td height="34"><div align="right" class="style2">Ime</div></td>
              <td><label>
                <input type="text" name="ime" />
              </label></td>
            </tr>
            <tr>
              <td height="33"><div align="right" class="style2">Starost</div></td>
              <td><label>
                <input type="text" name="starost" />
              </label></td>
            </tr>
            <tr>
              <td><div align="right" class="style2">Pol</div></td>
              <td><label>
                <input type="checkbox" name="muski" value="checkbox" />
                M<br />
                <input type="checkbox" name="zenski" value="checkbox" />
                Z</label></td>
            </tr>
            <tr>
              <td height="37"><div align="right" class="style2">Rodovnik</div></td>
              <td><label>
                <input type="file" name="rodovnik" />
              </label></td>
            </tr>
            <tr>
              <td height="32"><div align="right" class="style2">Fotografije</div></td>
              <td><label>
                <input type="file" name="foto" />
              </label></td>
            </tr>
            <tr>
              <td colspan="2"><label>
                <div align="center">
                  <input name="Submit2" type="submit" class="style1" value="Prijavi" />
                </div>
              </label></td>
              </tr>
          </table>
          </form>
        </div>  
      </td>
    </tr>

    <tr>
      <td></td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
</div>

<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>
