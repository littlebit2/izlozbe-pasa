<!DOCTYPE html>
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/dodavanjeClanka.css' ?>">
<title>Dodavanje članka</title>
</head>

<body>
<div class="header" id="header"><a href="<?php echo site_url('AdminController/index') ?>"><img src="<?php echo base_url().'images/home.png'?>" alt="home" width="50" height="50" border="0" /></a></div>
<?php echo validation_errors();?>
        
<?php echo form_open('DodavanjeClankaController/checkData'); ?>
  <table width="329" border="0" align="center">
    <caption align="top">
    <img src="<?php echo base_url().'images/plusLogo.png'?>" width="100" height="100" alt="add" />    <br />
      <br />
      Dodavanje članka<br />
    </caption>
    <tr>
      <td width="98">Naslov</td>
      <td width="215"><input type="text" name="naslovClanka" /></td>
    </tr>
    <tr>
      <td height="146">Tekst</td>
      <td><label>
        <textarea name="tekst" cols="50" rows="10"></textarea>
        </label>      </td>
    </tr>
    <tr>
      <td colspan="3"><div align="center">
        <input name="potvrdi" id="dugme" type="submit" value="Potvrdi" />
      </div></td>
    </tr>
  </table>
  
</form>
<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>
