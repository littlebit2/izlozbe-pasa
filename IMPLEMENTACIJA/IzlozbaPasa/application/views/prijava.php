<!DOCTYPE html>
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/prijava.css' ?>">
<title>Prijava</title>
</head>

<body>

<?php echo validation_errors(); ?>
<?php echo form_open('PrijavaController/proveri'); ?>
 <?php  error_reporting(E_ALL & ~E_NOTICE); ?>
<table width="78%" border="0">
  <tr>
    <td width="55%"><a href=<?php echo site_url('HomeController/index') ?>><img src=<?php echo base_url().'images/home.png'?> width="50" height="50" border="0" /></a></td>
    <td width="23%">&nbsp;</td>
    <td width="22%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center"><img src=<?php echo base_url().'images/userLogo.jpg'?> width="138" height="137" /></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="56">&nbsp;</td>
    <td><div align="left" class="style2"> 
      <div align="center">PRIJAVA</div>
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="32"><div align="right" class="style1">Korisničko ime </div></td>
    <td><label>
        <div align="left">
          <input type="text" name="username"  size="30" maxlength="30" id="username" />
        </div>
      </label>
	      </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="51"><div align="right" class="style1">Lozinka</div></td>
    <td><label>
        <div align="left">
          <input type="password" name="password" size="30" maxlength="30" id="password"/>
        </div>
      </label>   </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="38">&nbsp;</td>
    <td>
	<label>        
        <div align="left" class="style4">
          <div align="center">
            <input type="checkbox" name="checkbox" value="checkbox" accesskey="zapamti" id="zapamti"/>
            Zapamti me</div>
        </div>
      </label>   </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
	<label>        
        <div align="center">
          <input name="Submit" type="submit" class="dugme" accesskey="prijavi" value="Prijavi se" />
        </div>
      </label>    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="98">&nbsp;</td>
    <td><div align="left" class="style4">
     
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="left" class="style4">
      <div align="center">Nemate nalog? </div>
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <label>
        
        <div align="center">
          <input name="Submit2" type="submit" class="dugme" accesskey="registracija" value="Registracija" />
        </div>
      </label>   </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>
