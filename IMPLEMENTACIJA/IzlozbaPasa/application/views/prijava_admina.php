<?php
    session_unset();
    session_destroy();
?>

<!DOCTYPE html>
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/prijavaAdmina.css' ?>">
<title>Prijava admina</title>
</head>

<body>
<!--<form id="formaPrijavaAdmina" name="formaPrijavaAdmina" method="post" action="">-->

<?php echo validation_errors(); ?>    
<?php echo form_open('PrijavaAdminaController/proveri');  ?>
<table width="78%" border="0">
  <tr>
    <td width="46%"><a href="<?php echo site_url('HomeController/index') ?>"><img src=<?php echo base_url().'images/home.png'?> width="50" height="50" border="0" /></a></td>
    <td width="39%">&nbsp;</td>
    <td width="15%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center"><img src=<?php echo base_url().'images/lockLogo.jpg'?> width="100" height="100" /></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="197">&nbsp;</td>
    <td><div align="left" class="style2"> 
      <div align="center">PRIJAVA ADMINISTRATORA </div>
    </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="32"><div align="right" class="style1">Korisničko ime</div></td>
    <td>
      <label>
      <div align="left">
        <label>
        <input type="text" name="username" size="50" maxlength="50" id="username"/>
        </label>
      </div>
      </label>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="51"><div align="right" class="style1">Lozinka</div></td>
    <td>
      <label>
      <div align="left">
        <label>
        <input  type="password" name="password" size="50" maxlength="50" id="password"/>
        </label>
      </div>
      </label>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="38">&nbsp;</td>
    <td>
      <label>
      <div align="left" class="style4">
        <div align="center">
          <input type="checkbox" name="checkbox" value="checkbox" accesskey="zapamti" id="zapamti"/>
          Zapamti me</div>
      </div>
      </label>
      <div align="center"></div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <label>

        <div align="center">
          <input name="Submit" type="submit" id="dugme" class="style1" accesskey="prijavi" value="Prijavi se" />
          </div>
        </label>
   </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="98">&nbsp;</td>
    <td><div align="left" class="style4"></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <label>
      <div align="left"></div>
      </label>
   </td>
    <td>&nbsp;</td>
  </tr>
  
</table>
</form>
<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>
