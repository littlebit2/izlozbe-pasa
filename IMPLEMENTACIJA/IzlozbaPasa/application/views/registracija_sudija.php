<!DOCTYPE html>
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/regSudija.css' ?>">
<title>Registracija_sudija</title>

</head>

<body>
<?php //<form id="formaRegistracijaSudije" name="form2" method="post" enctype="multipart/form-data" action="">?>
    
<?php echo validation_errors();?>
<?php echo form_open('RegistracijaSudijaController/checkData'); ?>
    
<table width="80%" border="0" align="center">
  <tr>
      <td width="25%"><a href="<?php echo site_url('HomeController/index') ?>"><img src="<?php echo base_url().'images/home.png'?>" width="50" height="50" border="0" /></a></td>
    <td width="25%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
    <td width="25%">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><div align="center"><img src="<?php echo base_url().'images/editLogo.png'?>" width="70" height="70" /></div></td>
  </tr>
  <tr>
    <td colspan="4"><div align="center">REGISTRACIJA</div></td>
  </tr>
  <tr>
    <td><div align="right">Ime <span class="style1">*</span></div></td>
    <td>
      <input name="ime" type="text" size="35" id="imeSudije"/>
        </td>
    <td><div align="right">Prezime<span class="style1"> *</span></div></td>
    <td>
      <input name="prezime" type="text" size="35" id="prezimeSudije"/>
    </td>
  </tr>
  <tr>
    <td><div align="right">E-mail <span class="style1">*</span></div></td>
    <td>
      <input name="email" type="text" size="35" id="mejlSudije"/>
    </td>
    <td><div align="right">Korisničko ime<span class="style1"> *</span></div></td>
    <td><input name="username" type="text" size="35" /></td>
  </tr>
  <tr>
    <td><div align="right">Lozinka <span class="style1">*</span></div></td>
    <td>
      <input name="pass1" type="text" size="35" id="lozinkaSudije"/>
    </td>
    <td><div align="right">Ponovite lozinku <span class="style1">*</span></div></td>
    <td>
      <input name="pass2" type="text" size="35" id="ponovljenaLozinkaSudije"/>
    </td>
  </tr>
  <tr>
    <td><div align="right">Broj licence <span class="style1">*</span></div></td>
    <td>
      <input name="brlicence" type="text" size="35" id="brLicenceSudije"/>
    </td>
    <td><div align="right">Licenca <span class="style1">*</span></div></td>
    <td>
      <input type="file" name="licenca" id="licencaSudije"/>
    </td>
  </tr>
  <tr>
    <td colspan="4"><div align="center"><span class="style1">*</span> Polja označena zvezdicom su obavezna</div></td>
  </tr>
  <tr>
    <td colspan="4">
      <div align="center">
        <input type="checkbox" name="slazeSe" value="checkbox" id="slazeSeSudija"/>
        Slažem se sa uslovima korišćenja      </div>
        </td>
  </tr>
  <tr>
    <td colspan="4">
      <div align="center">
        <input type="checkbox" name="zeliObav" value="checkbox" id="zeliObavestenja" />
        Želim da dobijam obaveštenja na mail      </div>
    </td>
  </tr>
  <tr>
    <td colspan="4">
      <div align="center">
        <input type="submit" name="Submit" value="Registruj se! " />
        </div>
    </td>
  </tr>
  <tr>
    <td colspan="4"><div align="center"><a href="/IzlozbaPasa/index.php/PrijavaController/index">Već posedujete nalog? </a></div></td>
  </tr>
  <tr>
    <td colspan="4">
    </td>
  </tr>
</table>
</form>
<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>

