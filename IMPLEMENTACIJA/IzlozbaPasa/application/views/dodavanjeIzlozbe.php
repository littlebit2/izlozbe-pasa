<!DOCTYPE html>
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/dodavanjeIzlozbe.css' ?>"/>
<title>Dodavanje izložbe</title>
</head>

<body>
<div class="header" id="header"><a href="<?php echo site_url('HomeController/index'); ?>"><img src="<?php echo base_url().'images/home.png'?>" alt="home" width="50" height="50" border="0" /></a></div>
<?php echo validation_errors();?>
        
<?php echo form_open('DodavanjeIzlozbeController/checkData'); ?>

  <div align="center">
    <p>
      <label></label>
      <label></label>
      <label></label>
      <label><br />
      </label>
    <img src="<?php echo base_url().'images/plus.png'?>" width="100" height="100" alt="add" />    </p>
    <p>Dodavanje izložbe </p>
    <table width="329" border="0">
      <caption align="top">&nbsp;
      </caption>
      <tr>
        <td width="98">Naziv izložbe</td>
        <td width="215"><input type="text" name="nazivIzlozbe" /></td>
      </tr>
      <tr>
        <td>Datum</td>
        <td><input type="date" name="datumIzlozbe" /></td>
      </tr>
      <tr>
        <td>Lokacija</td>
        <td><input type="text" name="lokacijaIzlozbe" /></td>
      </tr>
      <tr>
        <td>Kategorije učesnika</td>
        <td><input type="radio" name="kategorije" checked="checked" id="radio1" value="grupa1"/>
          GRUPA I<br />
          <input type="radio" name="kategorije" id="radio2" value="grupa2"/>
          GRUPA II<br />
          <input type="radio" name="kategorije" id="radio3" value="grupa3"/>
          GRUPA III<br />
          
      </tr>
    </table>
    <p>
      <label>
        <input id="dugme" type="submit" name="potvrdi" value="Potvrdi" />
        <br />
        <br />
      </label>
    </p>
  </div>
</form>
<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>
