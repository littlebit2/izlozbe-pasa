<!DOCTYPE html >
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/regKorisnik.css' ?>">
<title>Registracija_korisnika</title>
</head>

<body>
<?php //<form id="formaRegistracijaKorisnika" name="forma" method="post" action=""> ?>
    
<?php echo validation_errors();?>
<?php echo form_open('RegistracijaKorisnikaController/checkData'); ?>

    
<table width="80%" border="0" align="center">
  <tr>
    <td width="30%"><a href=<?php echo site_url('HomeController/index') ?>><img src="<?php echo base_url().'images/home.png'?>" width="50" height="50" border="0" /></a></td>
    <td width="40%">&nbsp;</td>
    <td width="30%">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center"><img src="<?php echo base_url().'images/editLogo.png'?>" width="70" height="70" /></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="center">REGISTRACIJA</div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">Ime <span class="style1">*</span></div></td>
    <td>
      <input name="ime" type="text" size="57" id="imeKorisnika" />        </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">Prezime<span class="style1"> *</span></div></td>
    <td><input type="text" name="prezime" size="57" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">E-mail <span class="style1">*</span></div></td>
    <td>
      <input name="email" type="text" size="57" id="prezimeKorisnika"/>     </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">Korisničko ime <span class="style1">*</span> </div></td>
    <td>
      <input name="username" type="text" size="57" id="mejlKorisnika"/>    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">Lozinka <span class="style1">*</span></div></td>
    <td><input name="pass1" type="text" size="57" id="lozinkaKorisnika"/></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right">Ponovite lozinku <span class="style1">*</span> </div></td>
    <td>
      <input name="pass2" type="text" size="57" id="ponovljenaLozinkaKorisnika"/>    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"></div></td>
    <td><div align="center"><span class="style1">*</span> Polja označena zvezdicom su obavezna </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"></div></td>
    <td>
      <div align="center">
        <input type="checkbox" name="slazeSe" value="checkbox" id="slazeSeKorisnik"/>
        Slažem se sa uslovima korišćenja      </div>    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"></div></td>
    <td>
      <div align="center">
        <input type="checkbox" name="zeliObav" value="checkbox" id="zeliObavestenja"/>
        Želim da dobijam obaveštenja na mail      </div>    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"></div></td>
    <td>
      <div align="center">
        <input type="submit" name="Submit" value="Registruj se!" />
        </div>    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><div align="right"></div></td>
    <td><div align="center"><a href="/IzlozbaPasa/index.php/PrijavaController/index">Već posedujete nalog? </a></div></td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>
