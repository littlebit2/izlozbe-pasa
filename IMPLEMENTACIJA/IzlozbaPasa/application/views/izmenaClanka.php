<!DOCTYPE html>
<html lang="sr">
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'css/dodavanjeClanka.css' ?>">
<title>Izmeni članak</title>
</head>

<body>
<div class="header" id="header"><a href="<?php echo site_url('AdminController/index') ?>"><img src="<?php echo base_url().'images/home.png'?>" alt="home" width="50" height="50" border="0" /></a></div>
<div class="body">

<?php echo validation_errors();?>    
<?php error_reporting(E_ERROR|E_WARNING | E_PARSE); echo form_open($url); ?>
  <table width="329" border="0" align="center">
    <caption align="top">
    <img src="<?php echo base_url().'images/edit1.png'?>" alt="edit" width="100" height="100" />
          <br />
          <br />
    Izmena članka<br /> 
    </caption>
    <tr>
      <td width="98">Naslov</td>
      <td width="215"><input type="text" name="naslovClanka" /></td>
    </tr>
    <tr>
      <td height="146">Tekst</td>
      <td> <label>
          <textarea name="tekst" cols="50" rows="10"></textarea>
          </label> </td>
    </tr>
    
	<tr>
      <td colspan="3"><div align="center">
        <input name="potvrdi" id="dugme" type="submit" value="Potvrdi" />
      </div></td>
    </tr>
    </table>
</form>
</div>

<div id="footer"> <p align="center"> &#64; Copyright 2018 Little Bit</p></div>
</body>
</html>
