<?php
/** 
 *  Lazar Igić 389/2015ž
 *  Katarina Čegar 358/2014
 * DodavanjeClankaModel – klasa koja dodaje odredjeni članak
 * 
 * @version 1.0  
 */

class DodavanjeClankaModel extends CI_Model {
    
    public function dodajClanak($naslov, $text, $datum, $idAdmina) {
        
        $data = array('naslov'=>$naslov, 'text' => $text, 'datum'=>$datum, 'idAdmina'=>$idAdmina);

          $this->db->insert('clanak', $data);
   
    }
    
    
}