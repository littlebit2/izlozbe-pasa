<?php
//Aleksa Nedeljkovic 604/14
//Klasa omogcava registraciju sudija (unosenje u bazu i dohvaranje po username-u i password-u)
class RegistracijaSudijaModel extends CI_Model
{
    //unosenje sudije u bazu
    public function registracijaSudija($ime, $prezime, $email, $username, $pass1, $brlicence, $licenca, $zeli)
    {
        $data = array(
           'ime' => $ime,
           'prezime' => $prezime,
           'email' => $email,
           'korisnickoIme' => $username,
           'lozinka' => $pass1,
           'brojLicence' => $brlicence,
           'licenca' => $licenca,
           'zeli' => $zeli
        );
        
        $this->db->insert('sudija',$data);
    }
    
    //dohvatanje sudije iz baze sa unetim username-om i password-om radi dodeljivanja sesije
    public function dohvatiIdSudije($user, $pass) 
   {   
        session_unset();
        
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $result = $mysqli->query("select idSudija from sudija where korisnickoIme = '$user' and lozinka='$pass'");
        if ( (mysqli_num_rows($result)) ==1) {
                $row=$result->fetch_array();
                return $row["idSudija"];
            }
    }  
}
