<?php
//Aleksa Nedeljkovic 604/14
//Klasa omogcava registraciju korisnika (unosenje u bazu i dohvaranje po username-u i password-u)
class RegistracijaKorisnikaModel extends CI_Model
{ 
    //unosenje korisnika u bazu
    public function registracijaKorisnika($ime, $prezime, $email, $username, $pass1, $zeli)
    {
        $data = array(
           'ime' => $ime,
           'prezime' => $prezime,
           'email' => $email,
           'korisnickoIme' => $username,
           'lozinka' => $pass1,
           'zeli' => $zeli
        );
  
        $this->db->insert('korisnik',$data);
    }
    //dohvatanje korisnika iz baze sa unetim username-om i password-om radi dodeljivanja sesije
    public function dohvatiIdKorisnika($user, $pass) 
   {   
        session_unset();
        
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $result = $mysqli->query("select idKorisnik from korisnik where korisnickoIme = '$user' and lozinka='$pass'");
        if ( (mysqli_num_rows($result)) ==1) {
                $row=$result->fetch_array();
                return $row["idKorisnik"];
            }
    }
    
}