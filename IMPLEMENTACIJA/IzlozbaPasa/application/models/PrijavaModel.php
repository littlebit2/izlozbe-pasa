<?php
/** 
 *  Lazar Igić 389/2015ž
 *  Katarina Čegar 358/2014
 * PrijavaModel – klasa koja sluzi za prijavu i dohvatanje ID korisnika i sudija
 * 
 * @version 1.0  
 */
class PrijavaModel extends CI_Model {
    
    
    public function prijava($user, $pass) {
        $this->db->select('korisnickoIme ,lozinka');
        $this->db->from('korisnik');
        $this->db->where('korisnickoIme', $user);
        $this->db->where('lozinka', $pass);
        
        $query= $this->db->get();
        
        if($query->num_rows()== 1) {
            return true;
        } else {
            return false;
        }
    }
    
    public function prijavaSudije($user, $pass) {
        $this->db->select('korisnickoIme ,lozinka');
        $this->db->from('sudija');
        $this->db->where('korisnickoIme', $user);
        $this->db->where('lozinka', $pass);
        
        $query= $this->db->get();
        
        if($query->num_rows()== 1) {
            return true;
        } else {
            return false;
        }
    }

    
    public function dohvatiIdKor($user, $pass) {
        session_unset();
       
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $result = $mysqli->query("select idKorisnik from korisnik where korisnickoIme = '$user' and lozinka='$pass'");
        if ( (mysqli_num_rows($result)) ==1) {
                $row=$result->fetch_array();
                return $row["idKorisnik"];
            }
        
        
    } 
    
    public function dohvatiIdSud($user, $pass) {
        session_unset();
       
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $result = $mysqli->query("select idSudija from sudija where korisnickoIme = '$user' and lozinka='$pass'");
        if ( (mysqli_num_rows($result)) ==1) {
                $row=$result->fetch_array();
                return $row["idSudija"];
            }
        
        
    } 
    
    
    
    
    
}

