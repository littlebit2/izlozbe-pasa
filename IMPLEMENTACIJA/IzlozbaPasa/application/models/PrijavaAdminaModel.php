<?php
/** 
 *  Katarina Čegar 358/2014
 * PrijavaAdminaModel – klasa koja sluzi za prijavu i dohvatanje ID administratora
 * 
 * @version 1.0  
 */

class PrijavaAdminaModel extends CI_Model {
    
    
    public function prijava($user, $pass) {
        $this->db->select('korisnickoIme ,lozinka');
        $this->db->from('admin');
        $this->db->where('korisnickoIme', $user);
        $this->db->where('lozinka', $pass);
        
        $query= $this->db->get();
               
        if($query->num_rows()== 1) {
            
            return true;
        } else {
            return false;
        }
    }
    
    public function dohvatiIdAdmina($user, $pass) {
        session_unset();
       
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $result = $mysqli->query("select idAdmin from admin where korisnickoIme = '$user' and lozinka='$pass'");
        if ( (mysqli_num_rows($result)) ==1) {
                $row=$result->fetch_array();
                return $row["idAdmin"];
            }
        
        
    }
    
}

