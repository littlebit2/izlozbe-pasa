<?php
/** 
 *  Andrić Valerija 610/14
 * Igić Lazar 389/15
 * OcenjivanjeKorisnikController – klasa za ucitavanje podataka o psima za ocenjivanje
 * 
 * @version 1.0  
 */

class OcenjivanjeKorisnikController extends CI_Controller{
    /**  
 * Index funkcija – ucitava sve pse koji su prijavljeni za oodredjenu izlozbu
 * 
 * @return void  
 */
     public function index(){
         $this->load->model('Prijava');
            $idIzlozba=$this->uri->segment(3);
            
            $result1=$this->Prijava->findAll($idIzlozba);
            $result['rezultati']=array();
            $result['ocena']=array();
           
            while ($row=$result1->fetch_array()) {
                
                //array_push($result['rezultati'], $row);
                $this->load->model('Pas');
                $query=$this->Pas->getAll($row['idPsa']);
                $row+=$query->fetch_array();
                //array_push($result['rezultati'], $row);
                $this->load->model('OceneKorisnika');
                //$result['ocena']=$this->OceneKorisnika->prosek($row['idPsa']);
                $query=$this->OceneKorisnika->prosek($row['idPsa']);
                $row+=$query->fetch_array();
                array_push($result['rezultati'], $row);
                
                
            }
            //foreach($result['rezultati'] as $res){
              //  $this->load->model('Pas');
                //$query=$this->Pas->getAll($res['idPsa']);
                //$row2=$query->fetch_array();
               // array_push($result['rezultati'], $row2);
            //}
         $this->load->view('ocenjivanjeKorisnik', $result);    

         
    }
    
    
    
}

