<?php
/** 
 *  Andrić Valerija 610/14
 * OcenjivanjeController – klasa za ucitavanje izlozbi za korisnike
 * 
 * @version 1.0  
 */

class OcenjivanjeController extends CI_Controller{
/**  
 * Index funkcija – ucitava sve izlozbe iz baze
 * 
 * @return void  
 */
    public function index(){
         $this->load->model('Izlozba');
            
            $result1=$this->Izlozba->findAllNew();
            $result['rezultati']=array();
            
            while ($row=$result1->fetch_array()) {
                array_push($result['rezultati'], $row);
            }
          
        $this->load->view('ocenjivanje', $result);
    }
}

