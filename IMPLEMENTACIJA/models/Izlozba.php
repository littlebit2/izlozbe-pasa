
<?php
/** 
 *  Andrić Valerija 610/14
 * Izlozba – klasa komunikaciju sa bazom, tabela izlozba
 * 
 * @version 1.0  
 */

class Izlozba extends CI_Model {
/**  
 * FindAll funkcija – ucitava sve izlozbe
 * 
 * @return mysqli->query
 */  
    public function findAll(){
        $mysqli = new mysqli("localhost", "root", "", "mydb");
      
        return $mysqli->query("select * from izlozba");
    }
 /**  
 * FindAllNew funkcija – ucitava sve nove izlozbe
 * 
 * @return mysqli->query
 */ 
    
    public function findAllNew(){
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        $datum='2018-05-01';
        return $mysqli->query("select * from izlozba where datum>='$datum'");
        
    }
     /**  
 * ProveriNaziv funkcija – proverava  da li je ispravan naziv izlozbe
 * 
 * @return mysqli->query
 */ 
    
    public function proveriNaziv($naziv){
        $mysqli = new mysqli("localhost", "root", "", "mydb");
        return $mysqli->query("select naziv from izlozba");
        
    }
/**  
 * AddIzlozba funkcija – unosi novu izlozbu u bazu
 * 
 * @return void
 */ 

    public function addIzlozba($naziv, $datum, $lokacija, $kategorija){ 
            //$row=$this->db->count_all('izlozba');
            
            $data = array( 'naziv' => $naziv, 'datum'=>$datum, 'opis'=>$kategorija);

          $this->db->insert('izlozba', $data);
    }
/**  
 * UpdateIzlozba funkcija –  menja izlozbu u bazi
 * 
 * @return void
 */ 
    public function updateIzlozba($idIzlozba, $naziv,$datum, $lokacija, $kategorija){
        $data = array(
            'idIzlozba'=> $idIzlozba, 
            'naziv'=>$naziv, 
            'datum'=>$datum, 
            'opis'=>$kategorija);
        $this->db->where('idIzlozba', $idIzlozba);
        $this->db->update('izlozba', $data);
    }
    /**  
 * DeleteIzlozba funkcija –  brise zadatu izlozbu iz baze
 * 
 * @return void
 */ 
    
    public function deleteIzlozba($id){
        $this->db->delete('izlozba', array('idIzlozba' => $id)); 
        
    }
 }

         
