<?php
/** 
 *  Andrić Valerija 610/14
 * OceneSudija – klasa za podatake o ocenama sudija
 * 
 * @version 1.0  
 */

class OceneSudija extends CI_Model{
    /**  
 * FindAll funkcija – ucitava sve iz tabele ocene sudija
 * 
 * @return mysqli-> query
 */
public function findAll(){
        $mysqli = new mysqli("localhost", "root", "", "mydb");
      
        return $mysqli->query("select * from ocenesudija");// ne ovako!!!
    }
        /**  
 * AddOcena funkcija – unosi ocenu u tabelu ocene sudija
 * 
 * @return void
 */
    public function addOcena($idSudije,$idPsa,$ocena){
       
        
        
        $data = array( 'idSudije' => $idSudije,'idPsa'=>$idPsa, 'ocena'=>$ocena);

          $this->db->insert('ocenesudija', $data);
        
    }
    /**  
 * Prosek funkcija – vraca prosecnu ocenu za zadataog psa
 * 
 * @return mysqli-> query
 */
    public function prosek($idPsa){
        $mysqli = new mysqli("localhost", "root", "", "mydb");
      
        return $mysqli->query("select avg(ocena) as average from ocenesudija where idPsa=$idPsa");
          //$this->db->select_avg('ocena');
          //$this->db->where('idPsa', $idPsa); 
          //return $query = $this->db->get('ocenekorisnika');
    }
        /**  
 * Postoji funkcija – proverava da li postoji uneta ocena za odredjenog psa od strane odredjenog sudije
 * 
 * @return boolean
 */
  public function postoji($idSudije,$idPas){
      $mysqli = new mysqli("localhost", "root", "", "mydb");
      if($result=$mysqli->query("select idSudije,idPsa from ocenesudija where idPsa='$idPas' and idSudije='$idSudije'")){
          
      if($result->num_rows == 1){
            return true;
        }else{
            return false;
            
        }
      } 
  }
   /**  
 * RangLista funkcija – vraca 3 psa sa najvisim prosecnim ocenama
 * 
 * @return mysqli-> query
 */
  public function rangLista(){
      $mysqli = new mysqli("localhost", "root", "", "mydb");
      $mysqli->query("select top 3 idPsa,MAX(ocena) from ocenesudija group by idPsa, ocena order by ocena desc ");
         

  }
}