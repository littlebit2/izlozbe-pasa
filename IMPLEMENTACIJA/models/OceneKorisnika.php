<?php
/** 
 *  Andrić Valerija 610/14
 * Igić Lazar 389/15
 * OceneKorisnika – klasa za podatake o ocenama korisnika
 * 
 * @version 1.0  
 */

class OceneKorisnika extends CI_Model{
/**  
 * FindAll funkcija – ucitava sve iz tabele ocene korisnika
 * 
 * @return mysqli-> query
 */
    public function findAll(){
        $mysqli = new mysqli("localhost", "root", "", "mydb");
      
        return $mysqli->query("select * from ocenekorisnika");// ne ovako!!!
    }
    /**  
 * AddOcena funkcija – unosi ocenu u tabelu ocene korisnika
 * 
 * @return void
 */
    public function addOcena($idKor, $idPsa,$ocena, $datum){
        $data = array( 'idKorisnika' => $idKor,'idPsa'=>$idPsa, 'ocena'=>$ocena, 'datum'=>$datum);

          $this->db->insert('ocenekorisnika', $data);
        
    }
 /**  
 * Prosek funkcija – vraca prosecnu ocenu za zadataog psa
 * 
 * @return mysqli-> query
 */
    public function prosek($idPsa){
        $mysqli = new mysqli("localhost", "root", "", "mydb");
      
        return $mysqli->query("select avg(ocena) as average from ocenekorisnika where idPsa=$idPsa");
          //$this->db->select_avg('ocena');
          //$this->db->where('idPsa', $idPsa); 
          //return $query = $this->db->get('ocenekorisnika');
    }
    /**  
 * Postoji funkcija – proverava da li postoji uneta ocena za odredjenog psa od strane odredjenog korisnika
 * 
 * @return boolean
 */
  public function postoji($idKor,$idPas){
      $mysqli = new mysqli("localhost", "root", "", "mydb");
      if($result=$mysqli->query("select idKorisnika,idPsa from ocenekorisnika where idPsa='$idPas' and idKorisnika='$idKor'")){
          
      if($result->num_rows == 1){
            return true;
        }else{
            return false;
            
        }
      } 
      
      
  }
  /**  
 * RangLista funkcija – vraca 3 psa sa najvisim prosecnim ocenama
 * 
 * @return mysqli-> query
 */
  public function rangLista(){
      
      $mysqli = new mysqli("localhost", "root", "", "mydb");
      $mysqli->query("select top 3 idPsa,MAX(ocena) from ocenekorisnika group by idPsa, ocena order by ocena desc ");// treba da vrati 3 max prosecne ocene
         

  }
   
    
}

